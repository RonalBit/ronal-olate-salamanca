# Prueba técnica

Hola estimad@.

Te damos la bienvenida a la prueba técnica de Innovit.

## Aspectos generales
- Dispones de 210 minutos (3 horas y 30 minutos) para desarrollar lo solicitado y realizar la entrega.
- Se evaluará el uso de Git, Laravel, Vue y MySQL.

## Instrucciones

1. Realizar un **fork** del repositorio.
2. Desarrollar lo solicitado (ver sección *Desarrollos solicitados*).
3. Responder a las preguntas finales (ver sección *Preguntas finales*).
4. Solicitar un Pull Request desde su repositorio a la rama master de nuestro repositorio (Esto será considerado como prueba finalizada).

## Proyecto

El proyecto consiste en un sistema que almacena un listado de usuarios con sus respectivos roles.

### Base de datos

Consiste en 2 tablas (excluyendo migrations).

### Tabla roles

Almacena todos los roles, su estructura es:

- id
- slug
- nombre
- created_at
- updated_at

### Tabla usuarios

Almacena todos los usuarios, su estructura es:

- id
- email
- nombre
- password
- id_rol
- created_at
- updated_at

## Desarrollos solicitados

Para poder realizar los desarrollos, usted deberá primero crear las tablas de la base de datos y poblarlas de datos.
Para esto usted deberá ejecutar el comando:

`
php artisan migrate --seed
`

Tras su ejecución, se crearán 2 roles y 2 usuarios.

**Usuarios para probar**

| nombre      | email             | password | rol |
|-------------|-------------------|----------|----------| 
| Usuario Uno | usuario1@test.org | 123456   | Administrador |
| Usuario Dos | usuario2@test.org | 123456   | Usuario |

### Funcionalidades solicitadas

Usted deberá desarrollar las siguientes funcionalidades:

| Función      | Administrador             | Usuario |
|-------------|-------------------|----------|
| Listar usuarios | Sí, todos. | Sí, pero sólo su cuenta   |
| Agregar usuario | Sí, todos. | No   |
| Editar usuario | Sí, todos. | Sí, pero sólo su cuenta y no puede modificar su rol.   |
| Eliminar usuario | Sí, pero no su cuenta. | No   |


## Preguntas finales

Debido al éxito del proyecto, los nuevos clientes están solicitando nuevas y mejores funcionalidades.

Usted como líder del proyecto:

**¿Qué cambios realizaría en el modelo de datos para que una cuenta pueda tener más de un rol?**

Respuesta: Actualmente los modelos de rol y usuario se encuentran relacionados de uno a varios, en donde un usuario posee un unico rol. Para poder tener mas de un rol, habria que adicionar una tabla intermedia que me permita almacenar mas de un rol por usuario, ya que ahora la relacion seria de varios a varios entre usuario y y rol.

**¿Qué cambios realizaría para mejorar la seguridad del `login`?**

Respuesta: Adicionaria el uso de hash, por ejemplo apoyandonos con la funcion bcrypt. 

## Contacto

En caso de tener dudas durante la prueba, puede contactarse mediante:

- **Correo electrónico**: eduardo@innovit.cl
- **Whatsapp**: +56963530372


## Mucho éxito en la prueba


