<?php

use Illuminate\Database\Seeder;
use App\Rol;
use App\User;

class UsuariosTableSeeder extends Seeder
{
    /**
     * Ejecutar el Seeder.
     *
     * @return void
     */
    public function run()
    {
        $roles = Rol::pluck('id', 'slug');
        $usuarios = [
            [
                'email' => 'usuario1@test.org',
                'nombre' => 'Usuario Uno',
                'password' => bcrypt('123456'),
                'id_rol' => $roles->get('ADM')
            ],
            [
                'email' => 'usuario2@test.org',
                'nombre' => 'Usuario Dos',
                'password' => bcrypt('123456'),
                'id_rol' => $roles->get('USU')
            ]
        ];

        foreach ($usuarios as $usuario) {
            $user = User::where('email', '=', $usuario['email'])->first();
            if (is_null($user) && !is_null($usuario['id_rol'])) {
                $user = User::create($usuario);
                $user->save();
            }
        }
    }
}
