<?php

use Illuminate\Database\Seeder;
use App\Rol;

class RolesTableSeeder extends Seeder
{
    /**
     * Ejecutar el Seeder.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'ADM' => 'Administrador',
            'USU' => 'Usuario'
        ];

        foreach ($roles as $slug => $rol) {
            $ro = Rol::where('slug', '=', $slug)->first();
            if (is_null($ro)) {
                $ro = Rol::create([
                    'slug' => $slug,
                    'nombre' => $rol
                ]);
                $ro->save();
            }
        }
    }
}
