@extends('layouts.app')
@section('title','Usuarios')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><a href="{{route('usuarios.Gagregar')}}" title="Agregar" class="btn btn-success">Agregar</a> | Usuarios</div>

                    <div class="card-body" id="app">
                        <listar-usuarios
                            route-data="{!! $routeData !!}"
                            route-eliminar="{!! $routeEliminar !!}"
                            route-editar="{!! $routeEditar !!}"
                        ></listar-usuarios>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
