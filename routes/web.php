<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@indexPublico')->name('index.publico');

Auth::routes([
    'register' => false,
    'verify' => false,
    'reset' => false
]);

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    // Inicio
    Route::get('/', 'HomeController@indexPrivado')->name('index.privado');
    // Usuarios
    Route::group(['prefix' => 'usuarios'], function (){
        // Listar Usuarios
        Route::get('/','UsuariosController@index')->name('usuarios.index');
        Route::post('/listar','UsuariosController@listar')->name('usuarios.listar');
        // Agregar Usuario
        Route::get('/agregar','UsuariosController@Gagregar')->name('usuarios.Gagregar');
        Route::post('/agregar','UsuariosController@Pagregar')->name('usuarios.Pagregar');
        // Editar Usuario
        Route::get('/editar/{id?}','UsuariosController@Geditar')->name('usuarios.Geditar');
        Route::post('/editar','UsuariosController@Peditar')->name('usuarios.Peditar');
        // Eliminar Usuario
        Route::post('/eliminar', 'UsuariosController@eliminar')->name('usuarios.eliminar');
    });
});
