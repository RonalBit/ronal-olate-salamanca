<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    /**
     * Acciones al ingresar a la vista inicial del sistema (Sin sesión).
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function indexPublico()
    {
        return auth()->check()
            ? redirect()->route('index.privado')
            : redirect()->route('login');
    }

    /**
     * Vista inicial del sistema (con sesión)
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexPrivado()
    {
        return view('inicio');
    }
}
