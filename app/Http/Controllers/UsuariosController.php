<?php

namespace App\Http\Controllers;

use App\Http\Requests\AgregarUsuario;
use App\Http\Requests\EditarUsuario;
use Illuminate\Http\Request;
use App\User;
use App\Rol;

class UsuariosController extends Controller
{
    /**
     * Vista para listar todos los usuarios
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $routeData = route('usuarios.listar');
        $routeEliminar = route('usuarios.eliminar');
        $routeEditar = route('usuarios.Geditar');
        return view('usuarios.index', compact('routeData', 'routeEliminar', 'routeEditar'));
    }

    /**
     * Obtiene todos los usuarios a listar.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function listar(Request $request)
    {
        // COMPLETAR
        if(auth()->user()->rol->id == 1){
            $usuarios = User::with('rol')->get();
        }else {
            $usuarios = User::with('rol')->where('id',auth()->user()->id)->get();
        }
        return response()->json([
            // COMPLETAR
            'usuarios' =>$usuarios
        ]);
    }

    /**
     * Vista para agregar un usuario
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function Gagregar()
    {
        // COMPLETAR VALIDACIÓN PREVIA.

        $roles = Rol::select('id', 'nombre')->get();
        return view('usuarios.agregar', compact('roles'));
    }

    /**
     * Guarda un nuevo usuario.
     * @param AgregarUsuario $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function Pagregar(AgregarUsuario $request)
    {
        // COMPLETAR
        return redirect()->route('usuarios.index');
    }

    /**
     * Editar un usuario en específico.
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function Geditar($id)
    {
        // COMPLETAR
        return view('usuarios.editar');
    }

    /**
     * Guarda los cambios de un usuario en específico.
     * @param EditarUsuario $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function Peditar(EditarUsuario $request)
    {
        // COMPLETAR
        return redirect()->route('usuarios.index');
    }

    /**
     * Elimina un usuario.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function eliminar(Request $request)
    {
        // COMPLETAR
        return response()->json([
            // COMPLETAR
        ]);
    }
}
