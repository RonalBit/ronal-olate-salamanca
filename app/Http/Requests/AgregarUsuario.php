<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AgregarUsuario extends FormRequest
{
    /**
     * Determina si un usuario está autorizado para utilizar la validación
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->rol->slug == 'ADM' ?? false;
    }


    /**
     * Reglas de validación que serán aplicadas.
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:App\User,email',
            'password' => 'required|string|min:8',
            'rol' => 'required|exists:App\Rol,id'
        ];
    }
}
