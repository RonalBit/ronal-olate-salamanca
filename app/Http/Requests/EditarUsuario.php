<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditarUsuario extends FormRequest
{
    /**
     * Determina si un usuario está autorizado para utilizar la validación
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }


    /**
     * Reglas de validación que serán aplicadas.
     * @return array
     */
    public function rules()
    {
        return [
            // COMPLETAR
        ];
    }
}
